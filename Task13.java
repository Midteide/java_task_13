import java.util.ArrayList;

public class Task13 {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
    public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
    public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
    public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
    public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
    public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";

    public static String [][] colours = {{ANSI_BLACK, ANSI_RED, ANSI_GREEN, ANSI_YELLOW, ANSI_BLUE, ANSI_PURPLE, ANSI_CYAN,ANSI_WHITE}, {ANSI_BLACK_BACKGROUND, ANSI_RED_BACKGROUND, ANSI_GREEN_BACKGROUND, ANSI_YELLOW_BACKGROUND, ANSI_BLUE_BACKGROUND, ANSI_PURPLE_BACKGROUND, ANSI_CYAN_BACKGROUND,ANSI_WHITE_BACKGROUND}};


    public static class Person {
        // Making a class for storing Person data
        public Person (String firstname, String lastname) {
            this.fname = firstname;
            this.lname = lastname;
        }
        String fname() {
            return this.fname;
        }
        String fname;
        String lname;
    }

    public static void printInstructions() {
        System.out.println("Please provide arguments in following format: 'java Task13.java <search>'");
        System.out.println("Example: 'java Task13.java John'");
    }

    public static void main( String[] args ) {

        // Check if both arguments are provided
        if (args.length < 1) {
            printInstructions();
            System.exit(0) ;
        }
        if (args[0].length() == 0){
            printInstructions();
            System.exit(0) ;
        }
        // Creating 5 Person objects of class Person
        Person person1 =  new Person("John", "Smith");
        Person person2 =  new Person("Jane", "Doe");
        Person person3 =  new Person("Ola", "Nordmann");
        Person person4 =  new Person("Donald", "Duck");
        Person person5 =  new Person("Mickey", "Mouse");

        // Creating ArrayList 
        ArrayList<Person> persons = new ArrayList<Person>();
        persons.add(person1);
        persons.add(person2);
        persons.add(person3);
        persons.add(person4);
        persons.add(person5);

        System.out.println("___________________________________");
        System.out.println("All existing persons in list:");
        System.out.println("");
        for (Person p : persons) System.out.println("*  " + p.fname + " " + p.lname);
        System.out.println("___________________________________");
        boolean noMatch = true;

        for (Person p : persons) {
            if  ((p.lname.contains(args[0])) && (p.fname.contains(args[0]))) {
                System.out.println("Match for \"" + ANSI_YELLOW + args[0] + ANSI_RESET + "\" in both " + ANSI_CYAN +  "firstname and lastname" + ANSI_RESET + ": " + ANSI_GREEN + p.fname +  " " + ANSI_GREEN + p.lname + ANSI_RESET );
                noMatch=false;
                }
            else if  (p.lname.contains(args[0])) {
                System.out.println("Match for \"" + ANSI_YELLOW + args[0] + ANSI_RESET + "\" in " + ANSI_CYAN +  "lastname" + ANSI_RESET + ": " + p.fname + " " + ANSI_GREEN + p.lname + ANSI_RESET );
                noMatch=false;
            }
            else if (p.fname.contains(args[0])) {
                System.out.println("Match for \"" + ANSI_YELLOW + args[0] + ANSI_RESET + "\" in " + ANSI_PURPLE +  "firstname" + ANSI_RESET + ": " + ANSI_GREEN + p.fname + ANSI_RESET + " " + p.lname);
                noMatch=false;
            }
        }
        if (noMatch) {
            System.out.println("No match for search term \"" + ANSI_YELLOW + args[0] + ANSI_RESET + "\" ");
        }
        System.out.println("");
    }

    
}  